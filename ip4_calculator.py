import math #Jesse we need to cook, Jesse!


def main():
    repeat = True

    while repeat:
        ip_address     = input("\nGebe eine IP-Adresse ein: ")
        ip_address_arr = list()

        for ip in ip_address.split("."):
            if int(ip) >= 0 and int(ip) <= 255:
                ip_address_arr.append(ip)
            else:
                print((f"{ip_address} ist keine valide IP-Addresse. Eine valide IP-Addresse hat einen Zahlenbreich von 0-255."))
                exit()

        if ip_address[-1] != "0":
            print(f"{ip_address} ist kein Basisnetz. Die letzte Dezimalzahl eines Basisnetzes muss 0 sein.")
            exit()
        
        amount_subnet = int(input("In wie viele Subnetze soll das Netzwerk unterteilt werden: "))

        if amount_subnet < 0 or amount_subnet > 256:
            print(f"Eine 24er Maske kann maximal 256 Subnetze beinhalten.")
            exit()

        log_amount_subnets = math.log2(amount_subnet)
        
        while log_amount_subnets % 1 != 0:
            amount_subnet += 1
            log_amount_subnets = math.log2(amount_subnet)
            
        amount_hosts = 2**(8-log_amount_subnets)
        real_host_amount = amount_hosts - 2
        subnet_mask = [2**i for i in range(7,8-amount_subnet,-1)]
        print(f"Subnetz-Makse: 255.255.255.{sum(subnet_mask)}")

        print(f"Anzhal der Hosts pro Subnetz: {amount_hosts}")
        print(f"Anzahl der nutzbaren Hosts: {real_host_amount}")

        for i in range(amount_subnet):
            print(f"Netz {i+1}: {'.'.join(ip_address_arr[:-1])}.{int(ip_address_arr[-1]) + i*int(amount_hosts)}")

        ask_repeat = input("Erneut berechnen? [y/n]: ")

        repeat = True if ask_repeat == "y" else False
        


if __name__ == "__main__":
    main()
